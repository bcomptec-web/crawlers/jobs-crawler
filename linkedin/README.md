# linkedin-jobs-crawler

- Linkedin Jobs crawler built with [Scrapy](http://scrapy.org/). 
- Scrapes [Linkedin jobs](https://www.linkedin.com/jobs) and stores them in an SQLite database.

## Dependencies

### Production
- [scrapy](https://scrapy.org/)
- [scrapy-splash](https://github.com/scrapy-plugins/scrapy-splash)
- [w3lib](https://github.com/scrapy/w3lib)

### Development
- Selenium and Chromium Web Driver
- Twisted

```
cd jobs-crawler/linkedin
python -mvenv venv
. venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Run

### Spider with no login required

#### Modify Linkedin parameters  
Inside `linkedin_crawler/settings.py` specify:  
- LINKEDIN_SEARCH_URLS  

### Spider with login required

#### Run Splash
`docker run -p 8050:8050 scrapinghub/splash`

#### Modify Linkedin parameters  
Inside `linkedin_crawler/settings.py`specify:  
- LINKEDIN_USERNAME  
- LINKEDIN_PASSWORD  
- LINKEDIN_SEARCH_KEYWORDS  

### Command line
Run `scrapy crawl linkedin_nologin` or `scrapy crawl linkedin_login`

### PyCharm
- Create a new Run/Debug configuration
    - Script path: `jobs-crawler/linkedin/venv/lib/python3.7/site-packages/scrapy/cmdline.py`
    - Parameters: `crawl linkedin_nologin` or `crawl linkedin_login`
    - Execution: Enable checkbox `Run with Python console` (otherwise only debug will work)
    - Environment variables: `PYTHONUNBUFFERED=1`

## Visualize data
Use [SQLite Browser](https://sqlitebrowser.org/) or [DBeaver](https://dbeaver.io/) to open the `linkedin_jobs_search_results.db` database and run SQL queries from `queries.sql`.
