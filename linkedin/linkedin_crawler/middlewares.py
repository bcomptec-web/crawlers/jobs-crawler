import functools

import selenium
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
from scrapy.http import HtmlResponse
import os
import logging

from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem, HardwareType, SoftwareType, Popularity

logger = logging.getLogger(__name__)


class RandomUserAgentMiddleware(object):

    def __init__(self, crawler):
        super(RandomUserAgentMiddleware, self).__init__()
        self.user_agent_rotator = RandomUserAgentMiddleware.get_user_agent_rotator()
        self.fallback = crawler.settings.get('USER_AGENT', None)

    @staticmethod
    def get_user_agent_rotator():
        """
        https://github.com/Luqman-Ud-Din/random_user_agent
        :return: string user agent
        """
        # you can also import SoftwareEngine, HardwareType, SoftwareType, Popularity from random_user_agent.params
        # you can also set number of user agents required by providing `limit` as parameter

        software_names = [SoftwareName.CHROME.value,
                          SoftwareName.FIREFOX.value,
                          SoftwareName.SAFARI.value,
                          SoftwareName.EDGE.value,
                          SoftwareName.OPERA.value,
                          ]

        operating_systems = [OperatingSystem.WINDOWS.value,
                             OperatingSystem.LINUX.value,
                             OperatingSystem.MACOS.value,
                             OperatingSystem.FREEBSD.value,
                             OperatingSystem.OPENBSD.value,
                             ]
        hardware_types = [
            HardwareType.COMPUTER.value,
        ]

        software_types = [
            SoftwareType.WEB_BROWSER.value,
        ]

        popularity = [
            Popularity.POPULAR.value,
            Popularity.COMMON.value,
            Popularity.AVERAGE.value,
        ]

        return UserAgent(software_names=software_names,
                         operating_systems=operating_systems,
                         hardware_types=hardware_types,
                         software_types=software_types,
                         popularity=popularity,
                         limit=20000)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def process_request(self, request, spider):
        user_agent = self.user_agent_rotator.get_random_user_agent()
        spider.log('User agent: {}'.format(user_agent), level=logging.INFO)
        request.headers.setdefault('User-Agent', user_agent)

# source: https://stackoverflow.com/a/24373650


def check_spider_middleware(method):
    @functools.wraps(method)
    def wrapper(self, request, spider):
        msg = '%%s %s middleware step' % (self.__class__.__name__,)
        if self.__class__ in spider.middleware:
            spider.log(msg % 'executing', level=logging.DEBUG)
            return method(self, request, spider)
        else:
            spider.log(msg % 'skipping', level=logging.DEBUG)
            return None

    return wrapper


class SeleniumDownloaderMiddleware(object):

    def __init__(self):
        self.driver = self._create_google_chrome_driver()

    def _create_google_chrome_driver(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))

        from sys import platform
        is_windows = platform in ["win32", "win64"]

        chrome_driver_path = current_dir + os.sep + ('chromedriver.exe' if is_windows else 'chromedriver')

        chrome_options = webdriver.ChromeOptions()
        if is_windows:
            chrome_options.binary_location = 'C:\Program Files\Google\Chrome Beta\Application\chrome.exe'
        else:
            # google-chrome-stable
            # google-chrome-beta
            # google-chrome-unstable
            chrome_options.binary_location = '/usr/bin/google-chrome-unstable'

        # TODO: Recreate driver after TimeoutException with a different user_agent
        # 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
        # 'Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0'
        # 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0'
        user_agent = 'Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405'
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument(f'--user-agent={user_agent}')
        # chrome_options.add_argument('--enable-logging')
        # chrome_options.add_argument('--dump-dom')

        # Proxy
        # myProxy = "10.0.x.x:yyyy"
        # chrome_options.add_argument('--proxy-server=%s' % myProxy )
        # if os.geteuid() == 0:
        log_path = '--log-path=chromedriver.log'
        return webdriver.Chrome(executable_path=chrome_driver_path, options=chrome_options,
                                service_args=['--verbose', log_path])

    @check_spider_middleware
    def process_request(self, request, spider):
        self.driver.get(request.url)
        return HtmlResponse(request.url, encoding='utf-8', body=self.driver.page_source.encode('utf-8'))
