# encoding: utf-8
from __future__ import unicode_literals

from scrapy import signals
from scrapy.signalmanager import SignalManager
from scrapy.responsetypes import responsetypes
from scrapy.xlib.pydispatch import dispatcher
from selenium import webdriver
from six.moves import queue
from twisted.internet import defer, threads
from twisted.python.failure import Failure
import os
import logging


class SeleniumWebDriverDownloadHandler(object):

    def __init__(self, settings):
        self.options = settings.get('WEB_DRIVER_OPTIONS', {})

        max_run = settings.get('WEB_DRIVER_MAXRUN', 5)
        self.sem = defer.DeferredSemaphore(max_run)
        self.queue = queue.LifoQueue(max_run)

        SignalManager(dispatcher.Any).connect(self._close, signal=signals.spider_closed)

    def download_request(self, request, spider):
        """use semaphore to guard a driver pool"""
        return self.sem.run(self._wait_request, request, spider)

    def _create_google_chrome_driver(self, **options):
        current_dir = os.path.dirname(os.path.realpath(__file__))

        from sys import platform
        is_windows = platform in ["win32", "win64"]

        chrome_driver_path = current_dir + os.sep + ('chromedriver.exe' if is_windows else 'chromedriver')

        chrome_options = webdriver.ChromeOptions()
        if is_windows:
            chrome_options.binary_location = 'C:\Program Files\Google\Chrome Beta\Application\chrome.exe'
        else:
            # google-chrome-stable
            # google-chrome-beta
            # google-chrome-unstable
            chrome_options.binary_location = '/usr/bin/google-chrome-stable' # 74.0.3729.131 

        # TODO: Recreate driver after TimeoutException with a different user_agent
        # 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
        # 'Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0'
        # 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0'
        # 'Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405'
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36'
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument(f'--user-agent={user_agent}')
        chrome_options.add_argument('--user-data-dir=/home/ali/Software/chrome-stable-profile')
        # chrome_options.add_argument('--enable-logging')
        # chrome_options.add_argument('--dump-dom')

        # Proxy
        # myProxy = "10.0.x.x:yyyy"
        # chrome_options.add_argument('--proxy-server=%s' % myProxy )
        # if os.geteuid() == 0:
        log_path = '--log-path=chromedriver.log'
        return webdriver.Chrome(executable_path=chrome_driver_path, options=chrome_options,
                                service_args=['--verbose', log_path])

    def _wait_request(self, request, spider):
        try:
            driver = self.queue.get_nowait()
        except queue.Empty:            
            driver = self._create_google_chrome_driver(**self.options)

        logging.debug(f"Driver URL: {request.url}")

        driver.get(request.url)
        
        # ghostdriver won't response when switch window until page is loaded
        dfd = threads.deferToThread(lambda: driver.switch_to.window(driver.current_window_handle))
        dfd.addCallback(self._response, driver, spider)
        return dfd

    def _response(self, _, driver, spider):
        body = driver.execute_script("return document.documentElement.innerHTML")
        if body.startswith("<head></head>"):  # cannot access response header in Selenium
            body = driver.execute_script("return document.documentElement.textContent")
        url = driver.current_url
        cookies = driver.get_cookies()
        respcls = responsetypes.from_args(url=url, body=body[:100].encode('utf8'))
        resp = respcls(url=url, body=body, encoding="utf-8")

        response_failed = getattr(spider, "response_failed", None)
        if response_failed and callable(response_failed) and response_failed(resp, driver):
            driver.close()
            return defer.fail(Failure())
        else:
            self.queue.put(driver)
            return defer.succeed(resp)

    def _close(self):
        while not self.queue.empty():
            driver = self.queue.get_nowait()
            driver.close() 
