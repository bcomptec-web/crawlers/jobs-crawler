# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urlparse, unquote, urlencode, urlunparse, parse_qs

from scrapy import FormRequest
from scrapy.http import Request
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from linkedin_crawler.items import LinkedinCrawlerJobPageItem, LinkedinCrawlerSearchPageItem
from w3lib.html import replace_escape_chars
from scrapy.loader.processors import Compose, TakeFirst
from scrapy.utils.response import open_in_browser
import math
import scrapy_splash
from scrapy.utils.project import get_project_settings


class LinkedinSpiderLogin(scrapy.Spider):
    name = "linkedin_login"
    allowed_domains = ["linkedin.com"]
    login_url = 'https://www.linkedin.com/login'
    login_submit_url = 'https://www.linkedin.com/checkpoint/lg/login-submit'
    signed_out_pagination_url = 'https://www.linkedin.com/jobs-guest/jobs/api/jobPostings/jobs?' \
                                'redirect=false' \
                                '&position=1' \
                                '&pageNum=0' \
                                '&sortBy=DD' \
                                '&start=0'

    lua_cookies_script = """
        -- https://learnxinyminutes.com/docs/lua/
        function main(splash)
          splash:init_cookies(splash.args.cookies)
          
          assert(splash:go{
            splash.args.url,
            headers=splash.args.headers,
            http_method=splash.args.http_method,
            body=splash.args.body,
            })
          assert(splash:wait(0.5))
        
          local entries = splash:history()
          
          if entries[#entries] ~= nil then
            local last_response = entries[#entries].response
          
              return {
                url = splash:url(),
                headers = last_response.headers,
                http_status = last_response.status,
                cookies = splash:get_cookies(),
                html = splash:html(),
              }
          else
             return {
                url = splash:url(),
                headers = {},
                http_status = 200,
                cookies = splash:get_cookies(),
                html = splash:html(),
              }
          end
        end
    """

    def start_requests(self):
        yield Request(self.login_url, self.login, dont_filter=True)

    def login(self, response):
        csrf_token = response.xpath('//*[@name="csrfToken"]/@value').extract_first()
        login_csrf_param = response.xpath('//*[@name="loginCsrfParam"]/@value').extract_first()
        s_id_string = response.xpath('//*[@name="sIdString"]/@value').extract_first()
        ac = response.xpath('//*[@name="ac"]/@value').extract_first()
        control_id = response.xpath('//*[@name="controlId"]/@value').extract_first()
        parent_page_key = response.xpath('//*[@name="parentPageKey"]/@value').extract_first()
        page_instance = response.xpath('//*[@name="pageInstance"]/@value').extract_first()
        trk = response.xpath('//*[@name="trk"]/@value').extract_first()

        data = {
            'session_key': self.settings.get('LINKEDIN_USERNAME'),
            'session_password': self.settings.get('LINKEDIN_PASSWORD'),
            'csrfToken': csrf_token,
            'loginCsrfParam': login_csrf_param,
            'ac': ac,
            'sIdString': s_id_string,
            'controlId': control_id,
            'parentPageKey': parent_page_key,
            'pageInstance': page_instance,
            'trk': trk,
            'session_redirect': '',
            'fp_data': 'default',
            '_d': 'd',
        }
        self.logger.debug(f'Login data: {data}')
        splash_meta = {
            'splash': {
                'cache_args': ['lua_source'],
                'args': {
                    # set rendering arguments here
                    'html': 1,
                    'png': 0,

                    'wait': 0.5,

                    # 'url' is prefilled from request url
                    # 'http_method' is set to 'POST' for POST requests
                    # 'body' is set to request body for POST requests
                    'lua_source': self.lua_cookies_script
                },
                'endpoint': 'execute',
                'session_id': '42',
            }
        }

        yield FormRequest(url=self.login_submit_url, formdata=data, callback=self.after_login, meta=splash_meta)

    def after_login(self, response):
        # jobs_url = response.xpath('//*[@id="jobs-nav-item"]/a/@href')
        if response.status == 200:
            self.logger.info(f"Successfully logged in! Current URL: {response.url}")
            for url in self.settings.get('LINKEDIN_SEARCH_URLS'):
                self.logger.info(f'Crawling {url}')
                splash_meta = {
                    'splash': {
                        'cache_args': ['lua_source'],
                        'args': {
                            # set rendering arguments here
                            'html': 1,
                            'png': 0,

                            'wait': 0.5,

                            # 'url' is prefilled from request url
                            # 'http_method' is set to 'POST' for POST requests
                            # 'body' is set to request body for POST requests
                            'lua_source': self.lua_cookies_script
                        },
                        'endpoint': 'execute',
                        'session_id': '42',
                    }
                }
                yield Request(url,
                              callback=self.parse,
                              meta=splash_meta,
                              dont_filter=True,
                              headers={'Accept-Language': 'en-US,en;q=0.9'})
        else:
            # TODO: Parse error messages: https://github.com/linkedtales/scrapedin/blob/master/src/login.js#L27
            pass

    def parse(self, response):
        hxs = Selector(response)

        number_of_results = hxs.xpath('//*/span[@class="results-context-header__job-count"]/text()').get()
        if not number_of_results:
            number_of_results = hxs.xpath('//*/span[@class="results-context-header__job-total"]/text()').get()
            if not number_of_results:
                try:
                    number_of_results = \
                        hxs.xpath('//div[@class="display-flex t-12 t-black--light t-normal"]/text()').get().strip().replace('\n',
                                                                                                               '').split(
                            ' ')[0]
                except IndexError as ex:
                    self.logger.warning("Can't fetch results count")

        number_of_pages = 0
        number_of_items_per_page = 25  # first page seems to have 24 results
        if number_of_results:
            try:
                number_of_results = int(number_of_results)
            except ValueError:
                # FIXME: When there are more than 1000 results:
                # ValueError: invalid literal for int() with base 10: '1,000+'
                # Set to an arbitrary large value
                number_of_results = 1500

            number_of_pages = int(math.ceil(number_of_results / number_of_items_per_page))

        self.logger.info(f'Parse function called on {response.url},'
                         f' number_of_results = {number_of_results}, '
                         f'number_of_pages = {number_of_pages}')

        # page 0: https://www.linkedin.com/jobs/search/?keywords=android&location=Romania&sortBy=DD&start=0
        # page 1: https://www.linkedin.com/jobs/search/?keywords=android&location=Romania&sortBy=DD&start=25
        # page 3: https://www.linkedin.com/jobs/search/?keywords=android&location=Romania&sortBy=DD&start=50

        page_offset = 0
        for page_number in range(0, number_of_pages):
            self.logger.info(f"Page #{page_number + 1}")
            # We already have result for first page, parse it
            if page_number == 0:
                for result in self.parse_search_page_items(response):
                    yield result
                continue

            page_offset += number_of_items_per_page

            u = urlparse(response.url)
            query = parse_qs(u.query)
            query['start'] = page_offset
            query['pageNum'] = page_number
            u = u._replace(query=urlencode(query, True))
            next_page_url = urlunparse(u)

            self.logger.info(f"Next page URL: {next_page_url}")

            splash_meta = {
                'splash': {
                    'cache_args': ['lua_source'],
                    'args': {
                        # set rendering arguments here
                        'html': 1,
                        'png': 0,

                        'wait': 0.5,

                        # 'url' is prefilled from request url
                        # 'http_method' is set to 'POST' for POST requests
                        # 'body' is set to request body for POST requests
                        'lua_source': self.lua_cookies_script
                    },
                    'endpoint': 'execute',
                    'session_id': '42',
                }
            }

            yield Request(next_page_url, callback=self.parse_search_page_items, meta=splash_meta, dont_filter=True)

    def parse_search_page_items(self, response):
        self.logger.info(f"Parsing search results for page: {response.url}")
        jobs_selectors = response.xpath('//li[contains(@class, "result-card job-result-card")]')
        if not jobs_selectors:
            jobs_selectors = response.xpath('//li[contains(@class, "artdeco-list__item")]')
        for job_selector in jobs_selectors:
            yield self.parse_search_page_item(response, job_selector)

    def parse_search_page_item(self, response, job_selector):
        item_loader = ItemLoader(item=LinkedinCrawlerSearchPageItem(), response=response)
        item_loader.default_output_processor = Compose(TakeFirst(), lambda v: v.strip(), replace_escape_chars)

        # Keywords
        u = urlparse(response.url)
        query = parse_qs(u.query)
        keywords = ''.join(query['keywords']).strip()

        # Company name
        company_name = job_selector.xpath(
            './/div[contains(@class, "result-card__contents job-result-card__contents")]/h4/a/text()').get()
        if not company_name:
            company_name = job_selector.xpath(
                './/div/h4[contains(@class, "result-card__subtitle job-result-card__subtitle")]/text()').get()
        if not company_name:
            company_name = job_selector.xpath(
                './/h4/a[contains(@class, "job-card-search__company-name-link")]/text()') \
                .get()
        if not company_name:
            company_name = 'null'

        # Job title
        job_title = job_selector.xpath(
            './/h3[contains(@class, "job-card-search__title")]/a/text()') \
            .get()

        if not job_title:
            job_title = job_selector.xpath('.//a/span/text()').get()

        if job_title:
            job_title = job_title.replace('<!---->', '').replace('\n', '')

        # Job URL
        job_url = job_selector.xpath('.//a[contains(@class, "result-card__full-card-link")]/@href').get()
        if not job_url:
            job_url = job_selector.xpath(
                './/h3[contains(@class, "job-card-search__title")]/a/@href') \
                .get()

        if job_url and not job_url.startswith('http'):
            job_url = 'https://www.linkedin.com' + job_url

        if not job_url:
            job_url = 'null'

        job_id = '0'
        if job_url:
            try:
                job_id = urlparse(job_url).path.split('/')[3]
                if '-' in job_id:
                    job_id = job_id.split('-')[-1]
            except IndexError:
                try:
                    job_id = urlparse(job_url).path.split('-')[-1]
                except IndexError:
                    pass

        if job_id == 'null':
            job_id = '0'

        # YYYY-MM-DD
        job_date = job_selector.xpath('.//time/@ datetime').get() or ''

        job_location = job_selector.xpath('.//span[@class="job-result-card__location"]/text()').get()
        if not job_location:
            job_location = job_selector.xpath(
                './/h5[contains(@class, "job-card-search__location")]/text()[2]') \
                .get()
            if job_location:
                job_location = job_location.replace('<!---->', '').replace('\n', '')
            else:
                job_location = ''

        job_description = job_selector.xpath('.//p[@class="job-result-card__snippet"]/text()').get()
        if not job_description:
            job_description = 'null'

        self.logger.debug(f"company_name: {company_name}\n"
                          f"job_id: {job_id}\n"
                          f"job_title: {job_title}\n"
                          f"job_url: {job_url}\n"
                          f"job_date: {job_date}\n"
                          f"job_location: {job_location}\n"
                          f"job_description: {job_description}\n")

        item_loader.add_value('page_url', response.url)
        item_loader.add_value('keywords', keywords)
        item_loader.add_value('company_name', company_name)
        item_loader.add_value('job_id', job_id)
        item_loader.add_value('job_title', job_title)
        item_loader.add_value('job_date', job_date)
        item_loader.add_value('job_location', job_location)
        item_loader.add_value('job_description', job_description)
        item_loader.add_value('job_url', job_url)

        return item_loader.load_item()
