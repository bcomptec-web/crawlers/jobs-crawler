# -*- coding: utf-8 -*-

# Scrapy settings for linkedin_crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

from scrapy_splash import SlotPolicy
from urllib.parse import quote

BOT_NAME = 'linkedin_crawler'

SPIDER_MODULES = ['linkedin_crawler.spiders']
NEWSPIDER_MODULE = 'linkedin_crawler.spiders'

ITEM_PIPELINES = {'linkedin_crawler.pipelines.SqliteStorePipeline': 300}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0'

LOG_LEVEL = 'INFO'

# https://hackernoon.com/how-to-crawl-the-web-politely-with-scrapy-15fbe489573d
# https://doc.scrapy.org/en/latest/topics/autothrottle.html
DOWNLOAD_DELAY = 10

# https://doc.scrapy.org/en/latest/topics/settings.html#concurrent-requests
CONCURRENT_REQUESTS = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 1

COOKIES_DEBUG = False
SPLASH_COOKIES_DEBUG = False

#DOWNLOADER_MIDDLEWARES = {'linkedin_crawler.middleware.SeleniumDownloaderMiddleware': 500}

DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    'linkedin_crawler.middlewares.RandomUserAgentMiddleware': 400,
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
}

#DOWNLOAD_HANDLERS = {
#    'http': 'linkedin_crawler.handlers.SeleniumWebDriverDownloadHandler',
#    'https': 'linkedin_crawler.handlers.SeleniumWebDriverDownloadHandler',
#}

SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
}

SPLASH_URL = 'http://localhost:8050'

# No way to slow down the scrapy speed?: https://github.com/scrapy-plugins/scrapy-splash/issues/104
# Concurrency is not handled properly: https://github.com/scrapy-plugins/scrapy-splash/issues/213

# It specifies how concurrency & politeness are maintained for Splash requests
SPLASH_SLOT_POLICY = SlotPolicy.SINGLE_SLOT

# DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'
HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'

LINKEDIN_USERNAME = '<TBD>'
LINKEDIN_PASSWORD = '<TBD>'


def get_url(keywords, location):
    return f"https://www.linkedin.com/jobs/search/?redirect=false&position=1&pageNum=0" \
        f"&keywords={keywords}" \
        f"&location={location}" \
        f"&sortBy=DD&start=0"


LINKEDIN_SEARCH_KEYWORDS = [
    'Qt',  # 28
    'QML',  # 6
    'Golang',  # 38
    #'Node',  # 245
    #'React',  # 440
    #'Angular',  # 514
    #'Vue',  # 105
    #'Flutter',  # 1
    #'Android',  # 356
    #'JNI',   # 5
    #'NDK',  # 5
    #'Kotlin',  # 21
    #'C++',  # 625
    #'Yocto',  # 6
    #'PostgreSQL',  # 220
    #'Django',  # 73
    #'Flask',  # 30,
    #'Ruby',  # 196
    #'TypeScript',
    #'Embedded',  # 466
    #'Microcontroller',
    #'MATLAB',  # 79
    #'Spring',  # 526
    # 'Python',  # 1097
    # 'JavaScript',  # 1413
    # 'Java',  # 1779
    # 'Linux',  # 1417
]

LINKEDIN_SEARCH_LOCATION = 'Romania'

LINKEDIN_SEARCH_URLS = [get_url(quote(keyword), quote(LINKEDIN_SEARCH_LOCATION)) for keyword in LINKEDIN_SEARCH_KEYWORDS]
