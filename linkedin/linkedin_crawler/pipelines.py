# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import hashlib
import sqlite3
from scrapy.http import Request
from scrapy.exceptions import DropItem
import logging


class SqliteStorePipeline(object):
    def __init__(self):
        self.connection = sqlite3.connect('linkedin_jobs_search_results.db', isolation_level=None)
        self.create_schema()

    def __del__(self):
        if self.connection:
            self.connection.close()

    def create_schema(self):
        cursor = self.connection.cursor()
        try:
            cursor.execute('''CREATE TABLE IF NOT EXISTS linkedin_search_results (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                page_url TEXT, 
                                keywords TEXT,
                                company_name TEXT,
                                job_id BIG INT,  
                                job_title TEXT, 
                                job_date DATE, 
                                job_location TEXT, 
                                job_description TEXT,
                                job_url TEXT,
                                itime BIG INT)
                            ''')
            cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS unique_record_index 
                              ON linkedin_search_results(company_name, job_id, job_title)
                           ''')
        except sqlite3.Error as e:
            logging.error("An SQLite error occurred:", e.args[0])
        finally:
            cursor.close()

    def process_item(self, item, spider):
        if not item['job_id'] or item['job_id'] == '0':
            raise DropItem("Incorrect input. Check XPath queries!")

        cursor = self.connection.cursor()

        try:
            job_id = int(item['job_id'])
            job_title = item['job_title']
            company_name = item['company_name']
            keywords = item['keywords']

            cursor.execute("""SELECT EXISTS(SELECT 1 FROM linkedin_search_results
                                            WHERE company_name = ? AND job_id = ? and job_title = ?)
                           """, (company_name, job_id, job_title))

            if not job_id or not cursor.fetchone()[0]:
                cursor.execute("""INSERT INTO linkedin_search_results (page_url, 
                                                                       keywords,
                                                                       company_name, 
                                                                       job_id,
                                                                       job_title, 
                                                                       job_date, 
                                                                       job_location,
                                                                       job_description,
                                                                       job_url,
                                                                       itime)
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, strftime('%s', 'now'))
                               """,
                               (item['page_url'],
                                keywords,
                                company_name,
                                job_id,
                                job_title,
                                item['job_date'],
                                item['job_location'],
                                item['job_description'],
                                item['job_url']))
            else:
                logging.warning(f"Item already exists - {company_name}, {job_id}, {job_title}")
                # cursor.execute("""UPDATE linkedin_search_results
                #     SET keywords = (keywords || "~" || ?)
                #     WHERE keywords <> ? AND keywords NOT LIKE ? AND company_name = ? AND job_id = ? and job_title = ?
                # """, (keywords, keywords, ('%~' + item['keywords'] + '%'), company_name, job_id, job_title))
        except sqlite3.Error as e:
            logging.error("An SQLite error occurred:", e.args[0])
        finally:
            cursor.close()

        return item
