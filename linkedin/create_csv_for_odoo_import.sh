#!/usr/bin/env bash

set -euo pipefail

# Columns
# "id", "name", "partner_name", "country_id", "city", "website", "x_technology_ids", "tag_ids", "user_id"

sqlite3 -separator $'\t' -readonly -header -bail linkedin_jobs_search_results.db 'select id, company_name as name, company_name as partner_name, "Romania" as country_id, replace(replace(job_location, ", RO", ""), ", Romania", "") as city, job_url as website, (select group_concat(kw.keywords, ",") from (select distinct keywords from linkedin_search_results where company_name = t.company_name) as kw) as x_technology_ids, "Linkedin Jobs" as tag_ids, "Emanuel" as user_id from linkedin_search_results as t group by company_name order by company_name, job_location;' > linkedin_jobs.csv

# keywords
sqlite3 -separator $'\t' -readonly -header -bail linkedin_jobs_search_results.db 'select keywords as id, keywords as x_name, 0 as x_color from (select distinct keywords from linkedin_search_results order by keywords) t' > linkedin_jobs_keywords.csv

exit 0
