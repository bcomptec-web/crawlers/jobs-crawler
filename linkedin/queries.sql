-- Count results per keyword
select count(*), keywords 
from linkedin_search_results 
group by keywords
order by 1 desc, 2 asc

DELETE FROM linkedin_search_results WHERE keywords LIKE '%~%'

-- Check if column contains 1 space
select *
from linkedin_search_results
where keywords LIKE '% %'

-- Check if trimmed version matches
select *
from linkedin_search_results
where trim(keywords) = 'Embedded'
order by job_location, job_date DESC

-- All C++ jobs sorted by location asc and date desc
select company_name,
		job_title,
		job_location,
		strftime("%d-%m-%Y", job_date) as job_date
from linkedin_search_results
where keywords = 'C++'
order by job_location asc, strftime("%s 00:00:00", job_date) desc

-- All jobs from Brasov
select company_name,
		job_title,
		job_location,
		strftime("%d-%m-%Y", job_date) as job_date
from linkedin_search_results
where job_location LIKE '%Bra_ov%'
order by job_location asc, strftime("%s 00:00:00", job_date) desc

-- Get all keywords
select keywords
from linkedin_search_results
group by keywords
order by 1

-- Trim keywords column values
UPDATE linkedin_search_results
SET keywords = TRIM(keywords)

UPDATE linkedin_search_results
SET keywords = 'Qt'
WHERE keywords = 'Qt~Qt'

CREATE UNIQUE INDEX unique_record_index 
ON linkedin_search_results(company_name, job_id, job_title)

UPDATE linkedin_search_results SET itime = strftime('%s', 'now') WHERE itime IS NULL