# StackOverflow Jobs crawler

Jobs are scraped from the official RSS feeds offered by StackOverflow.  
Data for each technology is stored in: `stackoverflow_*_jobs.csv`  
Number of jobs for technology per country can be found in: `stackoverflow_counts.txt`  

Data points:  
- published date  
- country   
- city  
- company  
- title 
- tags 
- link  
- id  
- updated date  
- inserted date  

## Usage
`python stackoverflow_jobs.py`

## Visualize data
Use [SQLite Browser](https://sqlitebrowser.org/) or [DBeaver](https://dbeaver.io/) to open the `stackoverflow_jobs.db` database and run SQL queries from `queries.sql`.
