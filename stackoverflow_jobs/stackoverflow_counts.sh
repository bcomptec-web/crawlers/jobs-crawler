#!/usr/bin/env bash

set -ueo pipefail

output_file="stackoverflow_counts.txt"

> "$output_file"

for data_file in data/*.csv; do
    echo "Processing $data_file"
    tech=$(echo "$data_file" | cut -d'_' -f2)
	echo "$tech" >> "$output_file"
	cat "$data_file" | q -w all -d$'\t' -H "SELECT country, COUNT(*) cnt FROM - GROUP by country ORDER BY cnt DESC" >> "$output_file"
	echo "" >> "$output_file"
done

echo "Number of remote jobs" >> "$output_file"
rg -ilc Remote data/stackoverflow_*_jobs.csv | sort -r -n -t ':' -k2,2 >> "$output_file"

cat "$output_file"

exit 0
