-- All jobs tagged with specific keyword
SELECT * 
FROM stackoverflow_jobs
WHERE job_tags LIKE '%python%'
ORDER BY country, city, company, published_date

-- All remote jobs
SELECT * 
FROM stackoverflow_jobs
WHERE technology = 'remote'
ORDER BY country, city, company, published_date

-- The most used tags in Stack Overflow remote jobs
SELECT ROW_NUMBER () OVER ( 
        ORDER BY 2 DESC, 1 ASC
    ) number, t.tag, t.count
FROM (	
	WITH RECURSIVE split(tag_name, rest) AS (
	  SELECT '', trim(job_tags) || ',' FROM stackoverflow_jobs WHERE technology = 'remote'
	  UNION ALL
	  SELECT trim(substr(rest, 0, instr(rest, ','))),
			 trim(substr(rest, instr(rest, ',')+1))
		FROM split
	   WHERE rest <> '')
	SELECT trim(tag_name) as tag, COUNT(tag_name) as count
	  FROM split 
	 WHERE tag_name <> ''
	 GROUP BY 1
	 ORDER BY 2 DESC, 1 ASC
) t

-- Distinct companies accepting remote jobs
select distinct upper(company) from stackoverflow_jobs where technology = 'remote'

select job_id as id, company as name, company as partner_name, country as country_id, city, job_url as website, (

-- Select data to be imported into Odoo CRM (credit: http://www.samuelbosch.com/2018/02/split-into-rows-sqlite.html)
select group_concat(technology, ",") from ( 
    WITH RECURSIVE split(job_id, tag, rest) AS ( 
                select job_id, "", trim(job_tags) || "," from stackoverflow_jobs WHERE company = t.company
                UNION ALL 
                select tag, substr(rest, 0, instr(rest, ",")), substr(rest, instr(rest, ",")+1) 
                FROM split WHERE rest <> ""
    ) 
    SELECT UPPER(SUBSTR(trim(tag), 1, 1)) || SUBSTR(trim(tag), 2) as technology FROM split WHERE tag <> "" 
    UNION select distinct UPPER(SUBSTR(trim(technology), 1, 1)) || SUBSTR(trim(technology), 2) as technology from stackoverflow_jobs where company = t.company
 ) q ) as x_technology_ids, 

"StackOverflow Jobs" as tag_ids, "User" as user_id from stackoverflow_jobs as t group by company order by company, country, city;

-- All distinct job_tags + technology column
WITH RECURSIVE split(job_id, tag, rest) AS ( 
			select job_id, "", trim(job_tags) || "," from stackoverflow_jobs WHERE job_id
			UNION 
			select tag, substr(rest, 0, instr(rest, ",")), substr(rest, instr(rest, ",")+1) 
			FROM split WHERE rest <> ""
) 
SELECT UPPER(SUBSTR(trim(tag), 1, 1)) || SUBSTR(trim(tag), 2) as technology FROM split t WHERE tag <> ""
UNION 
select distinct UPPER(SUBSTR(trim(technology), 1, 1)) || SUBSTR(trim(technology), 2) as technology from stackoverflow_jobs
order by technology
