#!/bin/bash

set -euo pipefail

declare -a technologies
technologies=(go python kotlin android flutter react-native c++ flask django docker kubernetes postgresql sql linux typescript reactjs angular vue.js node.js javascript php machine-learning)

# ------------------------------------------------------------------------------------------------------------------

echo "TOTAL jobs on StackOverflow for technologies"
declare -A all_jobs
for technology in "${technologies[@]}"
do
  number_of_results=$(xidel -s -e '//*/div[@id="index-hed"]/div/span[@class="description"]/text()' "https://stackoverflow.com/jobs?tl=$technology" | head -n 1 | cut -d' ' -f1)
  all_jobs[$technology]=$number_of_results
  #printf "%s has %s jobs\n" "$technology" "$number_of_results"
  sleep 2
done

for technology in "${!all_jobs[@]}"
do
  echo "$technology - ${all_jobs[$technology]}"
done | sort -k3,3rn -k1,1

# ------------------------------------------------------------------------------------------------------------------

echo -e "\nREMOTE jobs on StackOverflow for technologies"
declare -A remote_jobs
for technology in "${technologies[@]}"
do
  number_of_results=$(xidel -s -e '//*/div[@id="index-hed"]/div/span[@class="description"]/text()' "https://stackoverflow.com/jobs?r=true&tl=$technology" | head -n 1 | cut -d' ' -f1)
  #printf "%s has %s remote jobs\n" "$technology" "$number_of_results"
  remote_jobs[$technology]=$number_of_results
  sleep 2
done

for technology in "${!remote_jobs[@]}"
do
  echo "$technology - ${remote_jobs[$technology]}"
done | sort -k3,3rn -k1,1

# ------------------------------------------------------------------------------------------------------------------

exit 0
