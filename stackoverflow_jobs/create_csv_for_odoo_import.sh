#!/usr/bin/env bash

set -euo pipefail

# Columns
# "id", "name", "partner_name", "country_id", "city", "website", "x_technology_ids", "tag_ids", "user_id"

sqlite3 -separator $'\t' -readonly -header -bail stackoverflow_jobs.db 'select job_id as id, company as name, company as partner_name, country as country_id, city, job_url as website, (select group_concat(technology, ",") from ( WITH RECURSIVE split(job_id, tag, rest) AS ( select job_id, "", trim(job_tags) || "," from stackoverflow_jobs WHERE company = t.company UNION ALL select tag, substr(rest, 0, instr(rest, ",")), substr(rest, instr(rest, ",")+1) FROM split WHERE rest <> "") SELECT UPPER(SUBSTR(trim(tag), 1, 1)) || SUBSTR(trim(tag), 2) as technology FROM split WHERE tag <> "" UNION select distinct UPPER(SUBSTR(trim(technology), 1, 1)) || SUBSTR(trim(technology), 2) as technology from stackoverflow_jobs where company = t.company) q ) as x_technology_ids, "StackOverflow Jobs" as tag_ids, "Emanuel" as user_id from stackoverflow_jobs as t group by company order by company, country, city;' > stackoverflow_jobs.csv

# keywords
sqlite3 -separator $'\t' -readonly -header -bail stackoverflow_jobs.db 'select technology as id, technology as x_name, 0 as x_color from (select distinct UPPER(SUBSTR(technology, 1, 1)) || SUBSTR(technology, 2) as technology from stackoverflow_jobs order by technology) t' > stackoverflow_jobs_keywords.csv

exit 0
