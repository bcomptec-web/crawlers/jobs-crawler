#!/usr/bin/env python

#import requests
import feedparser
import time
import csv
import os
from urllib.parse import quote
import sys
import logging
from enum import Enum
import datetime
import sqlite3
import traceback

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(stream=sys.stdout, format=FORMAT, level=logging.INFO)
logger = logging.getLogger('stackoverflow_jobs')

"""
Inspiration: https://alvinalexander.com/python/python-script-read-rss-feeds-database

1. Download an RSS feed from the URL given on the command line.
2. Checks a database to see if the title of each feed is already in the database, 
and if so, if it was put in there more than 12 hours ago.
3. Prints only the “new” RSS feed titles.
4. For titles not already in the database, it writes the titles and timestamps to the database.

curl -s https://stackoverflow.com/jobs/feed?tl=qt | xmllint --format - > qt.rss
"""

POST_VALIDITY_TIME = (12 * 3600)  # 12 hours
OVERALL_DATA_VALIDITY_TIME = (1 * 86400)  # 1 day


class Utils(object):
    usa_states = {
        "AL": "Alabama",
        "AK": "Alaska",
        "AS": "American Samoa",
        "AZ": "Arizona",
        "AR": "Arkansas",
        "CA": "California",
        "CO": "Colorado",
        "CT": "Connecticut",
        "DE": "Delaware",
        "DC": "District Of Columbia",
        "FM": "Federated States Of Micronesia",
        "FL": "Florida",
        "GA": "Georgia",
        "GU": "Guam",
        "HI": "Hawaii",
        "ID": "Idaho",
        "IL": "Illinois",
        "IN": "Indiana",
        "IA": "Iowa",
        "KS": "Kansas",
        "KY": "Kentucky",
        "LA": "Louisiana",
        "ME": "Maine",
        "MH": "Marshall Islands",
        "MD": "Maryland",
        "MA": "Massachusetts",
        "MI": "Michigan",
        "MN": "Minnesota",
        "MS": "Mississippi",
        "MO": "Missouri",
        "MT": "Montana",
        "NE": "Nebraska",
        "NV": "Nevada",
        "NH": "New Hampshire",
        "NJ": "New Jersey",
        "NM": "New Mexico",
        "NY": "New York",
        "NC": "North Carolina",
        "ND": "North Dakota",
        "MP": "Northern Mariana Islands",
        "OH": "Ohio",
        "OK": "Oklahoma",
        "OR": "Oregon",
        "PW": "Palau",
        "PA": "Pennsylvania",
        "PR": "Puerto Rico",
        "RI": "Rhode Island",
        "SC": "South Carolina",
        "SD": "South Dakota",
        "TN": "Tennessee",
        "TX": "Texas",
        "UT": "Utah",
        "VT": "Vermont",
        "VI": "Virgin Islands",
        "VA": "Virginia",
        "WA": "Washington",
        "WV": "West Virginia",
        "WI": "Wisconsin",
        "WY": "Wyoming"
    }

    canada_states = {
        'AB': 'Alberta',
        'SK': 'Saskatchewan',
        'YT': 'Yukon Territory',
        'BC': 'British Columbia',
        'PE': 'Prince Edward Island',
        'NS': 'Nova Scotia',
        'NB': 'New Brunswick',
        'ON': 'Ontario',
        'QC': 'Quebec',
        'NL': 'Newfoundland and Labrador',
        'NU': 'Nunavut',
        'NT': 'Northwest Territories',
        'MB': 'Manitoba'
    }

    european_union_countries = ['Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark',
                                'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Ireland', 'Italy',
                                'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Poland', 'Portugal',
                                'Romania', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'United Kingdom']

    @staticmethod
    def normalize_country(country):
        country = country.replace('Deutschland', 'Germany').replace('Österreich', 'Austria') \
            .replace('Schweiz', 'Switzerland').replace('UK', 'United Kingdom').replace('Slowakei', 'Slovakia'). \
            replace('Czechia', 'Czech Republic')
        return country

    @staticmethod
    def get_formatted_timestamp(timestamp):
        return datetime.datetime.utcfromtimestamp(timestamp).strftime('%d.%m.%Y')

    @staticmethod
    def get_database_formatted_timestamp(timestamp):
        return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d')


class Database(object):
    def __init__(self, technology='None'):
        self.technology = technology

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def is_old_data(self):
        pass

    def save_post(self, post):
        pass

    def save_posts(self, posts):
        pass

    def post_is_in_db(self, post):
        pass

    def post_is_in_db_with_old_timestamp(self, post):
        pass


class DatabaseType(Enum):
    FILESYSTEM = 1
    SQLITE = 2


class FileSystemDatabase(Database):
    def __init__(self, technology):
        super(FileSystemDatabase, self).__init__(technology)
        self.file_path = f'data/stackoverflow_{technology}_jobs.csv'
        self.file_handle = None
        self.current_timestamp = int(time.time())

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close_file()

    def is_old_file(self):
        t = os.stat(self.file_path)
        c = t.st_ctime

        now = time.time()
        cutoff = now - OVERALL_DATA_VALIDITY_TIME
        if c < cutoff:
            return True

        return False

    def is_old_data(self):
        if self.file_exists():
            if not self.is_old_file():
                logger.info(f"File {self.file_path} is not old enough.")
                return False
            else:
                os.remove(self.file_path)
        logger.info(f"File is old: {self.file_path}")
        return True

    def file_exists(self):
        if not os.path.exists(self.file_path):
            return False
        return True

    def file_is_empty(self):
        if self.file_exists():
            return os.stat(self.file_handle).st_size == 0
        return True

    def open_file(self):
        if not self.file_handle:
            logger.debug(f"Opening file: {self.file_path}")
            self.file_handle = open(self.file_path, newline='', encoding='utf-8')
        else:
            logger.debug(f"Resetting file handle: {self.file_path}")
            self.reset_file_handle()

    def close_file(self):
        if self.file_handle:
            logger.debug(f"Closing file handle: {self.file_path}")
            self.file_handle.close()

    def reset_file_handle(self):
        if self.file_handle.tell() != 0:
            self.file_handle.seek(0)

    def post_is_in_db(self, post):
        if not self.file_exists():
            return False

        self.open_file()

        reader = csv.reader(self.file_handle, delimiter='\t', quotechar='"')
        for row in reader:
            if post['id'] == row[7]:
                return True

        return False

    # The job title is in the database with a timestamp > limit
    def post_is_in_db_with_old_timestamp(self, post):
        if not self.file_exists():
            return False

        self.open_file()

        reader = csv.reader(self.file_handle, delimiter='\t', quotechar='"')
        for row in reader:
            if post['id'] == row[7]:
                if self.current_timestamp - row[9] > POST_VALIDITY_TIME:
                    return True

        return False

    def save_posts(self, posts):
        with open(self.file_path, 'a', newline='', encoding='utf-8') as f:
            writer = csv.writer(f, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')
            if self.file_is_empty:
                writer.writerow(['published', 'country', 'city', 'company', 'title', 'tags', 'link', 'id',
                                 'updated', 'inserted'])
            for post in posts:
                if not self.post_is_in_db(post):
                    published = Utils.get_formatted_timestamp(post['published'])
                    updated = Utils.get_formatted_timestamp(post['updated'])
                    writer.writerow([published,
                                     post['country'],
                                     post['city'],
                                     post['company'],
                                     post['title'],
                                     post['tags'],
                                     post['link'],
                                     post['id'],
                                     updated,
                                     self.current_timestamp,
                                     ], )


class SqliteDatabase(Database):
    def __init__(self, technology='None'):
        super(SqliteDatabase, self).__init__(technology)
        self.connection = sqlite3.connect('stackoverflow_jobs.db', isolation_level=None)
        self.create_schema()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close_connection()

    def is_old_data(self):
        cursor = self.connection.cursor()
        try:
            # Empty table
            cursor.execute('SELECT COUNT(*) FROM stackoverflow_jobs')
            if cursor.fetchone()[0] == 0:
                return True

            cursor.execute('SELECT COUNT(*) FROM stackoverflow_jobs WHERE technology = ?', (self.technology,))
            if cursor.fetchone()[0] == 0:
                return True

            # Table with data
            cursor.execute("""SELECT EXISTS(SELECT 1 FROM stackoverflow_jobs 
                                            WHERE technology = ? 
                                            AND itime < (strftime('%s', 'now') - ?))
                              """, (self.technology, OVERALL_DATA_VALIDITY_TIME))
            return cursor.fetchone()[0] == 1
        except Exception as ex:
            logger.error(ex)
            logger.error(traceback.format_exc())
        finally:
            cursor.close()
        return False

    def save_post(self, post):
        if not self.post_is_in_db(post):
            return self.insert_post(post)
        else:
            return self.update_post(post)

    def save_posts(self, posts):
        for post in posts:
            self.save_post(post)

    def post_is_in_db(self, post):
        if not post:
            return False

        cursor = self.connection.cursor()
        try:
            cursor.execute("SELECT EXISTS(SELECT 1 FROM stackoverflow_jobs WHERE job_id = ?)", (post['id'],))
            return cursor.fetchone()[0] == 1
        except Exception as ex:
            logger.warning(ex)
            logger.error(traceback.format_exc())
        finally:
            cursor.close()

        return False

    def post_is_in_db_with_old_timestamp(self, post):
        if not post:
            return False

        cursor = self.connection.cursor()
        try:
            cursor.execute("""SELECT EXISTS(SELECT 1 FROM stackoverflow_jobs WHERE job_id = ? AND ((strftime('%s', 'now') - itime) > ?))
                              """, (post['id'], POST_VALIDITY_TIME))
            return cursor.fetchone()[0] == 1
        except Exception as ex:
            logger.error(ex)
            logger.error(traceback.format_exc())
        finally:
            cursor.close()

        return False

    def close_connection(self):
        if self.connection:
            self.connection.close()

    def create_schema(self):
        cursor = self.connection.cursor()
        try:
            cursor.execute('''CREATE TABLE IF NOT EXISTS stackoverflow_jobs (
                                published_date DATE,
                                updated_date DATE,
                                country TEXT,  
                                city TEXT,
                                company TEXT, 
                                job_id BIG INT PRIMARY KEY,
                                job_title DATE, 
                                job_tags TEXT, 
                                job_url TEXT,
                                feed_url TEXT, 
                                technology TEXT,
                                itime BIG INT)
                            ''')
        except sqlite3.Error as e:
            logger.error("An SQLite error occurred:", e.args[0])
            logger.error(traceback.format_exc())
            return False
        finally:
            cursor.close()
        return True

    def insert_post(self, post):
        cursor = self.connection.cursor()
        try:
            published = Utils.get_database_formatted_timestamp(post['published'])
            updated = Utils.get_database_formatted_timestamp(post['updated'])
            cursor.execute("""INSERT INTO stackoverflow_jobs (published_date, 
                                                              updated_date,
                                                              country, 
                                                              city, 
                                                              company,
                                                              job_id,
                                                              job_title,
                                                              job_tags,
                                                              job_url,
                                                              feed_url,
                                                              technology,
                                                              itime)
                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, strftime('%s', 'now'))
                               """,
                           (published,
                            updated,
                            post['country'],
                            post['city'],
                            post['company'],
                            post['id'],
                            post['title'],
                            post['tags'],
                            post['link'],
                            post['feed_url'],
                            post['technology'],
                            ))
        except sqlite3.Error as e:
            logger.error("An SQLite error occurred:", e.args[0])
            logger.error(traceback.format_exc())
            return False
        finally:
            cursor.close()
        return True

    def update_post(self, post):
        logger.warning(f"Item already exists - {post['company']}, {post['id']}, {post['title']}")
        return True


class StackOverflowCrawler(object):
    def __init__(self, database_type=DatabaseType.FILESYSTEM):
        self.database_type = database_type

    def dump_post(self, data):
        logger.debug(
            f"ID: {data['id']}, Title: {data['title']}, Company: {data['company']}, Location: {data['location']}, "
            f"Link: {data['link']}, Published: {data['published']}, Updated: {data['updated']}")

    def get_database(self, technology=None):
        if self.database_type == DatabaseType.FILESYSTEM:
            return FileSystemDatabase(technology)
        elif self.database_type == DatabaseType.SQLITE:
            return SqliteDatabase(technology)

    def run(self, urls, debug=False, sleep_time_s=5):
        number_of_urls = len(urls)
        should_nap = True if number_of_urls > 1 else False

        user_agent = 'StackOverflowJobsCrawler/1.0 +https://www.remoteonly.org/'
        feedparser.USER_AGENT = user_agent

        for tech, url in urls.items():
            with self.get_database(tech) as db:
                # Only run the crawler if data is old
                if not db.is_old_data():
                    logger.info(f"Data is not old enough for technology: {tech}. Moving on!")
                    continue

                logger.info(f"Fetching feed from: {url}")
                
                #response = requests.get(url)
                
                #if response.status_code == requests.codes.ok:
                  #print(response.content)
                feed = feedparser.parse(url)
                feed_name = feed['feed']['title']
                
                feed_total_results = feed['feed']['os_totalresults']
                logger.info(f"Feed: {feed_name} has {feed_total_results} entries")

                posts = self.get_posts(feed, tech, url)

                db.save_posts(posts)

                if debug:
                    for post in posts:
                        self.dump_post(post)
                #else:
                #  logger.warning(f"Cannot get data from url: {url}, status code: {response.status_code}", )
                logger.info(f"Napping for {sleep_time_s} seconds")

                if should_nap:
                    time.sleep(sleep_time_s)

    def get_posts(self, feed, technology, feed_url):
        posts = []
        # figure out which posts to print
        for post in feed.entries:
            # if post is already in the database, skip it

            allows_remote = 'remote' in post.title.lower()

            location = ''
            country = ''
            city = ''

            if hasattr(post, 'location'):
                location = post.location
                location_parts = post.location.split(', ')
                sz = len(location_parts)
                if sz > 0:
                    city = location_parts[0].strip()

                if sz > 1:
                    country = Utils.normalize_country(location_parts[1].strip())

                # e.g. Singapore
                if sz == 1:
                    country = city

                if len(country) == 2:
                    if country in Utils.usa_states:
                        country = 'United States of America'
                    elif country in Utils.canada_states:
                        country = 'Canada'

            if allows_remote and not country and not city:
                country = city = 'Remote'

            tags = ''
            if hasattr(post, 'tags'):
                tags = ', '.join([tag['term'] for tag in post.tags])

            data = {'id': post.id,
                    'title': post.title,
                    'company': post.author,
                    'location': location,
                    'city': city,
                    'country': country,
                    'link': post.link,
                    'published': int(time.mktime(post.published_parsed)),
                    'updated': int(time.mktime(post.updated_parsed)),
                    'tags': tags,
                    'technology': technology,
                    'feed_url': feed_url,
                    }
            posts.append(data)

        return sorted(posts, key=lambda k: (k['country'], k['city'], k['company'], k['published']))


if __name__ == '__main__':
    tags = [
        'android',
        'kotlin',
        'flutter',
        'react-native',
        'react',
        'vue.js',
        'c++',
        'c++11',
        'c++14',
        'c++17',
        'qt',
        'qml',
        'c',
        'go',
        'python',
        'django',
        'flask',
        'postgresql',
        'node.js',
    ]

    #urls = {tag: f'https://stackoverflow.com/jobs/feed?tl={quote(tag)}' for tag in tags}

    # NOTE: there is a limit of 1000 results and no pagination support
    # 5638 results in total
    # 25 results per page
    # => 225 pages + 1 (226 for some reason)
    # urls = {
    #   'all': 'https://stackoverflow.com/jobs/feed'
    # }
    crawler = StackOverflowCrawler(database_type=DatabaseType.SQLITE)

    remote_first = {'remote': 'https://stackoverflow.com/jobs/feed?r=true'}
    crawler.run(remote_first)

    #crawler.run(urls)
